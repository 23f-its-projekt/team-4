# team 4:

Her er projektplanen for semester projektet.  
Erik Westerberg - erwe26710@edu.ucl.dk  
Oscar Harttung - oafh28885@edu.ucl.dk  
Jens Christensen - jech54843@edu.ucl.dk

# Belbin:

![Belbin roller](./assets/belbin.jpg)

# Baggrund:

De studerende på IT Sikkerhed skal løbende i dette semester udarbejde et tværfagligt projekt som skal være inden for rammen af læringsmålene for fagene i dette semester.

Projekt danner grundlag for eksaminationer i fagene Introduktion til IT Sikkerhed,
System sikkerhed og Netværks- og kommunikations sikkerhed og inkluderer en rapport der bruges som aflevering til eksamen i System sikkerhed

# Formål:

Formålet er at danne et grundlag for IT-sikkerheds arbejde på tværs af de fag vi har i dette semester og forberede os på vores eksamener samt bachelor-projekt i fremtiden.
Projektet skal også så vidt muligt udfylde læringsmålene for de relevante fag og inddrage + skabe ny viden indenfor IT-sikkerhed.

# Mål:

Målet er at så vidt muligt skabe et realistisk system som vi kan bruge til at forstå og undersøge de forskellige sikkerheds aspekter. Vise hvordan forskellige services og systemer kan undersøges og forstærkes med fokus på sikkerhed, samt at bruge projektet til understøttelse af vores eksamener og viden indenfor faget.

# Problemformuelering:

Dette dokument danner grundlag for det fremadrettede sammenarbejde mellem team 4, og virksomheden HR-ON, som fremadrettet beskrives som kunden. Kunden er en softwarevirksomhed, som markedsfører to produkter inden for HR. Deres ældste system hedder Recruit, som benyttes til rekruttering af medarbejdere. Deres nyeste produkt hedder Staff som kan bruges til at styre hele HR processen fra onboarding til offboarding. I dag har kunden ikke nogen dedikeret IT-afdeling, som betyder at der ikke er nogen overordnet ansvarshaver i forhold til håndhævelsen af infrastrukturen. For at få en forståelse af hvordan situationen er for kunden i dag, vil første opgave være at få lavet en riskovudering, som skal kortlægge hvilke mulige svagheder der er i dag, og hvilken påvirkning de ville kunne forsage. Hvordan kan man forhindre uautoriserede personer i at få adgang til et it-system, og hvilke teknologier og procedurer kan implementeres for at øge sikkerheden og minimere risikoen for uautoriseret adgang?

## Problem:

Uautoriserede som skaber sig adgang til systemet.

## Universel påstand:

Implementering af en kombination af to-faktor autentificering, datakryptering og SIEM-overvågning vil føre til en signifikant reduktion i antallet af vellykkede uautoriserede adgangsforsøg til et it-system, sammenlignet med implementering af kun én af disse sikkerhedsforanstaltninger.
Denne hypotese antager, at kombinationen af disse tre sikkerhedsforanstaltninger vil øge sikkerheden og reducere risikoen for uautoriseret adgang.
Asymmetriske påstand:
Implementering af to-faktor autentificering alene vil være lige så effektivt i at reducere antallet af vellykkede uautoriserede adgangsforsøg som implementering af en kombination af to-faktor autentificering, datakryptering og SIEM-overvågning.
Denne hypotese antager, at to-faktor autentificering alene vil være lige så effektivt som en kombination af sikkerhedsforanstaltninger i at reducere risikoen for uautoriseret adgang.
Eksperiment:
For at teste denne hypotese kan man udføre en eksperimentel undersøgelse, hvor man sammenligner effektiviteten af implementeringen af to-faktor autentificering med implementeringen af en kombination af to-faktor autentificering, datakryptering og SIEM-overvågning.

## Need to have:

1. Flere afdelinger med brug af forskellige services vi kan tage udgangspunkt i
2. Opfyldelse af de væsentlige læringsmål
3. Læseligt og relevant dokumentation
4. Test af tekniske produkter
5. Udvikling af flere tekniske sikkerheds tiltag (Logging, segmentering etc.)
6. Færdig rapport til system sikkerhed eksamen
7. GitLab issues board brugt til opgaver
8. Netværksdiagram over virksomhed
9. Færdig Projekt Rapport

## Nice to have:

1. En sammenarbejds virksomhed
2. Mere detaljeret netværks segmentering
3. IT-Governance aspekter indraget (måske noget compliance fx.)
4. GitLab pages

# Tidsplan:

## uge 8

- Projektidé
- Projektplan
- Tids estimering

## uge 9

- Opstart af projekt
- Forståelse af idéer og "virksomhed" + afdelinger

## uge 10

- Begyndelse af implementeringer
- Første iteration af Network diagram færdig

## uge 11

- Vejledning/Milepæl med Martin + Nikolaj

## uge 16

- Vejledning/Milepæl med Martin + Nikolaj
- Fastlæggelse af problemformulering
- Systemsmål og systemskrav
- Overfører flere filer fra docs til GitLab

## uge 18

- Vejledning/Milepæl med Martin + Nikolaj

## uge 23

- eksamen 19+20 juni

# Organisation

Erik - Medlem  
Oscar - Medlem  
Jens - Medlem  
Martin - Vejleder  
Nikolaj - Vejleder

# Risikostyring

Hvilke risici er der ift. projektets fuldførelse og hvordan skal de håndteres hvis de opstår?

Lige nu usikker på 3.medlem som kan skabe problemer med arbejdsfordeling.

- 3.medlem siger om han er aktiv eller ej så vi bedre kan dele arbejdet.
  Langtidssygdom kunne udsætte arbejdet der bliver lavet.
- Enkelte Sygdomsdage bliver kommunikeret i gruppechat eller anden kommunikations form og ved længere varende må man afstemme hvad man skal gøre.
  Internt konflikt skaber dårlig stemning og lavt arbejd.
- Snakke om problemer før de skaber større konflikter, hvis ikke konflikt kan løses internt inddrage Nikolaj eller Martin til at skabe ro igen.
  Discord går ned og man kan ikke komme i kontakt med ens gruppe.
- bruge element eller mail som erstatning.

# Interessenter

Hvem er opdragsgiver?

- Nikolaj og Martin har udstedt projektet og betragtes derfor som opdragsgiver da de har sat rammerne for projektet og dens indhold.

Hvem skal anvende projektresultatet?

- De studerende kan selv anvende resultatet for projektet da det er en del af eksamen for system sikkerhed, men andre firmaer som overvejer hvad for nogen sikkerheds tiltag de skal vælge ville også kunne have gavn da idéen er at lave et firma som nemt kunne tilpasses.

Hvem skal acceptere projektresultatet?

- Ultimativt er det Nikolaj og Martin der skal acceptere resultatet for projektet da det er eksamenerne den skal bruges til primært, men hvis der bliver idraget viden fra et firma skal de også kunne acceptere vores resultater.

Hvem berøres (bemærker, generes, lider afsavn, får udbytte, får ændrede forhold)?

- De studerende bliver berørt med den nye indsamlet viden og erfaring, forhåbenligt bliver lærene også berørt ved at læse projektet og andre firmaer der vælger at læse/være med i projektet kunne også få noget udbytte.

Hvem leverer indsats, viden, kunnen, ressourcer til projektet?

- De studerende levere indsatsen og kunnen til projektet i form af at skrive den, med viden og ressourcer fra lærene i løbet af undervisning over semestret og muligt hjælp fra et firma som kunne levere ekspert viden og erfaringer fra virkeligeheden til projektet.

Hvem skal acceptere, at projektets aktiviteter udføres, og måden de gennemføres på?

- Lærene skal acceptere at de metoder der bliver brugt i projektet stemmer overens med det overordnet mål af projektet og de studerende skal også selv kunne stå inde for de aktiviteter der udføres.

# kommunikation

Discord - som udgangspunkt men mail og element kan også bruges hvis der er nedetid.  
Fysisk til undervisning etc.

## Kommunikationskanaler

- GitLab issue board
- Element
- Discord
- E-mail
- Sms/mobil ved nødstilfælde

## Kommunikationsaktiviteter

- Møder
- Periodiske emails
- Branching strategier
- Kommunikation med eksterne
- mm.

# Perspektiv

Projektet danner grundlag for uddannelsens øvrige semesterprojekter og skal opfattes som en øvelse i at lave et godt bachelor projekt som afslutning på uddannelsen.

# Evaluering

Hvordan evalueres projektet udover de obligatoriske eksamens afleveringer.  
Det anbefales at evaluere alle projekter ved afslutning for at synliggøre og samle erfaringer til brug i næste projekt.  
Det er også en god ide at, som team, at kigge tilbage på projektets forløb for at reflektere over hvad de enkelte teammedlemmer har lært i løbet af projektet.

Endelig bør et godt udført og veldokumenteret projekt inkluderes i en studerendes portfolio/dokumentation side så det er muligt for andre at få glæde af projektet.

# Referencer

Relevante overordnede referencer til essentielle ting der benyttes i projektet, hver reference bør have en beskrivelse der kort begrunder og forklarer hvordan referencer er relevant for projektet.

- Links til dokumentation
- Links til metoder
- mm.
